condition c ;
mutex m ;
file *f ;

void *thread_traite (void *arg) {
  lock (&m) ;
  while (file_vide(f)) {  // \alert{$\leftarrow$ boucle}
    cwait (&c, &m) ;
  }
  unlock (&m) ;
  traiter (extraire_file (f)) ;
}

void *thread_produit (void *arg) {
  d = lire_donnee (...) ;
  lock (&m) ;	// si besoin de section critique
  ajouter_file (f, d) ;
  csignal (&c) ;
  unlock (&m) ;
}
