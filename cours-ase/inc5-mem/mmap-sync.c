struct exemple {
    sem_t s ;				// un sémaphore...
    pthread_mutex_t m ;			// ... et/ou un mutex
    ...
} *p ;

p = mmap (...) ;

// initialisation d'un sémaphore
sem_init (&p->s, 1, 12) ;		// 1 pour partager entre processus

// initialisation d'un mutex avec l'attribut \frquote{pshared}
pthread_mutexattr_t ma ;		// attribut de mutex
pthread_mutexattr_init (&ma) ;		// initialisation de l'attribut de mutex
pthread_mutexattr_setpshared (&ma, 1) ;	// synchronisation entre processus
pthread_mutex_init (&p->m, &ma) ;	// initialisation du mutex avec l'attribut
pthread_mutexattr_destroy (&ma) ;	// on n'a plus besoin de l'attribut après l'initialisation du mutex
