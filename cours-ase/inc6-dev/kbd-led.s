// envoyer la commande ED (hexadécimal) au control register
// pour allumer les LED (qui doivent être spécifiées via
// l'input buffer)

    out  0xed,0x60

// attendre que l'input buffer soit vide (donnée d'une
// précédente commande)

boucle:
    in   0x64,%al	// registre al $\leftarrow$ status register
    and  0x02,%al	// al $\leftarrow$ status \& 0x02
    cmp  %al,0		// teste le bit$_1$ de al
    jne  boucle		// bit$_1$ $\neq$ 0 $\Rightarrow$ input buffer non vide

// spécifier les LED à allumer via les bits dans l'input buffer
// 0x01 : ScrollLock, 0x02 : NumLock, 0x04 : CapsLock

    out  0x06,0x60	// écrire dans l'input buffer
