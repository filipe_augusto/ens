// attendre que l'output buffer reçoive une donnée du clavier

attente:
    in   0x64,%al	// registre al $\leftarrow$ status register
    and  0x01,%al	// al $\leftarrow$ status \& 0x01
    cmp  %al,0		// teste le bit$_0$ de al
    jeq   attente	// bit$_0 = 0 \Rightarrow$ rien dans l'output buffer

// l'output buffer contient quelque chose : le lire

    in   0x60,%al 	// al $\leftarrow$ code de la touche
